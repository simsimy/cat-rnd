# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141221141943) do

  create_table "cats", force: true do |t|
    t.string   "url",           limit: 255
    t.string   "by",            limit: 255
    t.string   "license",       limit: 255
    t.string   "license_image", limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "by_url",        limit: 255
    t.string   "name",          limit: 255
    t.string   "source_url",    limit: 255
    t.boolean  "is_active",     limit: 1,   default: true
  end

  add_index "cats", ["is_active"], name: "index_cats_on_is_active", using: :btree
  add_index "cats", ["source_url"], name: "index_cats_on_source_url", using: :btree

end
