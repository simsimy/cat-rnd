class AddActiveToCats < ActiveRecord::Migration
  def change
    add_column :cats, :is_active, :boolean, default: true
  end
end
