class CreateCats < ActiveRecord::Migration
  def change
    create_table :cats do |t|
      t.string :url
      t.string :by
      t.string :license
      t.string :license_image

      t.timestamps null: false
    end
  end
end
