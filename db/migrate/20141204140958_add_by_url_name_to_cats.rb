class AddByUrlNameToCats < ActiveRecord::Migration
  def change
    add_column :cats, :by_url, :string
    add_column :cats, :name, :string
  end
end
