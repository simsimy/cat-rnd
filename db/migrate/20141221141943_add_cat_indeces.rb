class AddCatIndeces < ActiveRecord::Migration
  def change
    add_index :cats, :is_active
    add_index :cats, :source_url
  end
end
