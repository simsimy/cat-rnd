json.array!(@cats) do |cat|
  json.extract! cat, :id, :url, :by, :license, :license_image
  json.url cat_url(cat, format: :json)
end
