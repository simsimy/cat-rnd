
class ImportCatController < ApplicationController
  def index
    require 'open-uri'
    FlickRaw.api_key="038cc084285dc4c37fed1d5936a79cc3"
    FlickRaw.shared_secret="f003890d412f89af"
    url=''
    page=1
    while (page < 100)
      page = page+1
      fliJson=JSON.load(open("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=038cc084285dc4c37fed1d5936a79cc3&tags=cats&license=4%2C5%2C6&format=json&nojsoncallback=1&per_page=100&page=#{page}", {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}))
      @photos = fliJson["photos"]["photo"]

      @photos.map{ |p|

        photo =  flickr.photos.getInfo :photo_id => p["id"] #, :secret => p["secret"]
        sizes = flickr.photos.getSizes :photo_id => photo.id

        original = sizes.find {|s| s.label == 'Original' }
         if (original && original.width.to_i >  original.height.to_i && original.width.to_i > 1024)
          cat = Cat.new
          cat.by=photo.owner.realname.empty? ? photo.owner.username : photo.owner.realname
          cat.url = original.source
          #cat.url="https://farm#{photo.farm}.staticflickr.com/#{photo.server}/#{photo.id}_#{photo.secret}.jpg"
          cat.license= photo.license
          cat.by_url="https://www.flickr.com/people/#{photo.owner.nsid}"
          cat.name=photo.title

          cat.source_url= photo.urls.find{|u| u.type=='photopage'}._content
            unless Cat.exists?(:source_url=>cat.source_url)
              cat.save()
            end
         end
      }
      @import_cat=JSON.pretty_generate(fliJson)
    end
  end
end
